const {createApp} = Vue;
createApp({
    data(){
        return {
            randomIndex: 0,
            randomIndexInternet: 0,

            //Vetor de imagens locais
            imagensLocais:[
                './Imagens/lua.jpg',
                './Imagens/SENAI_logo.png',
                './Imagens/sol.jpg'
                
            ],

            imagensinternet:[

                "https://burst.shopifycdn.com/photos/perfect-yellow-flower.jpg?width=1200&format=pjpg&exif=1&iptc=1",

                "https://t.ctcdn.com.br/5XPASDBUosgmBv5Ptpxcd6eTJso=/512x288/smart/filters:format(webp)/i257652.jpeg",

                "https://a.travel-assets.com/findyours-php/viewfind…s/res40/476000/476389-South-Boston-Waterfront.jpg",
            ],


        };//Fim return
    },//Fim data

    computed:{
        randomImage()
        {
            return this.imagensLocais [this.randomIndex];

        },//fim randomImage
        
        randomImageInternet()
        {
            return this.imagensinternet [this.randomIndexInternet];

        }//Fim randomInternet
    },//Fim computed

    methods: {
        getRandomImage()
        {
            this.randomIndex = Math.floor(Math.random()*this.imagensLocais.length);

            this.randomIndexInternet = Math.floor(Math.random()*this.imagensinternet.length);
        }
    },//Fim methods
}).mount("#app");